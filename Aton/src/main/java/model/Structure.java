package model;

public class Structure {
    private final long account;
    private String name;
    private double value;

    public Structure(long account, String name, double value) {
        this.account = account;
        this.name = name;
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public long getAccount() {
        return account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Structure{" +
                "account=" + account +
                ", name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    public static Structure copy(Structure structure) {
        return new Structure(structure.getAccount(), structure.getName(), structure.getValue());
    }
}
