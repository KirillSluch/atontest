import model.Structure;
import repository.StructureRepository;
import repository.StructureRepositoryInMemoryImpl;

public class Main {
    public static void main(String[] args) {
        StructureRepository repository = new StructureRepositoryInMemoryImpl();

        Structure structure1 = new Structure(123456, "Transaction", 5647);
        Structure structure2 = new Structure(1234789, "Transaction", 5647);
        Structure structure3 = new Structure(123479, "Transaction2", 5647);
        repository.save(structure1);
        repository.save(structure2);
        repository.save(structure3);

        System.out.println(repository.findByAccount(123456L));
        System.out.println(repository.findByName("Transaction"));
        System.out.println(repository.findByValue(5647.0));

        structure1.setName("Updated transaction");
        repository.update(structure1);
        System.out.println(repository.findByAccount(123456L));

        repository.delete(structure1);
        System.out.println(repository.findByAccount(123456L));
    }
}
