package exception;

public class StructureNotFoundException extends IllegalArgumentException {
    public StructureNotFoundException(String message) {
        super(message);
    }
}
