package exception;

import model.Structure;

public class AlreadyExistsException extends IllegalArgumentException{
    public AlreadyExistsException(String message) {
        super(message);
    }
}
