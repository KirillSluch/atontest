package repository;

import model.Structure;

import java.util.Set;

public interface StructureRepository {
    Structure save(Structure structure);
    void delete(Structure structure);

    Structure update(Structure updateStructure);

    Structure findByAccount(Long account);

    Set<Structure> findByName(String name);

    Set<Structure> findByValue(Double value);
}
