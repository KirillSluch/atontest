package repository;

import exception.AlreadyExistsException;
import exception.StructureNotFoundException;
import model.Structure;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class StructureRepositoryInMemoryImpl implements StructureRepository {
    private static final Map<Long, Structure> db = new TreeMap<>();
    private static final Map<String, Set<Long>> nameIndex = new TreeMap<>();
    private static final Map<Double, Set<Long>> valueIndex = new TreeMap<>();

    @Override
    public Structure save(Structure structure) {
        Long account = structure.getAccount();
        checkNotContainsAccount(account);

        db.put(account, Structure.copy(structure));

        createIndexes(structure);

        return structure;
    }

    @Override
    public void delete(Structure structure) {
        Long account = structure.getAccount();
        String name = structure.getName();
        Double value = structure.getValue();

        checkContainsAccount(account);

        db.remove(account);
        nameIndex.get(name).remove(account);
        valueIndex.get(value).remove(account);
    }

    @Override
    public Structure update(Structure updateStructure) {
        Long account = updateStructure.getAccount();

        checkContainsAccount(account);
        clearIndexes(account);
        createIndexes(updateStructure);
        db.put(account, Structure.copy(updateStructure));

        return updateStructure;
    }

    private void addToNameIndex(Long account, String name) {
        if (nameIndex.containsKey(name)) {
            nameIndex.get(name).add(account);
        } else {
            Set<Long> set = new HashSet<>();
            set.add(account);
            nameIndex.put(name, set);
        }
    }

    private void addToValueIndex(Long account, Double value) {
        if (valueIndex.containsKey(value)) {
            valueIndex.get(value).add(account);
        } else {
            Set<Long> set = new HashSet<>();
            set.add(account);
            valueIndex.put(value, set);
        }
    }

    private void createIndexes(Structure structure) {
        Long account = structure.getAccount();
        String name = structure.getName();
        Double value = structure.getValue();

        addToNameIndex(account, name);
        addToValueIndex(account, value);
    }

    private void clearIndexes(Long account) {
        Structure oldStructure = db.get(account);
        String name = oldStructure.getName();
        Double value = oldStructure.getValue();

        nameIndex.get(name).remove(account);
        valueIndex.get(value).remove(account);
    }

    @Override
    public Structure findByAccount(Long account) {

        return db.get(account);
    }

    @Override
    public Set<Structure> findByName(String name) {
        Set<Long> accounts = nameIndex.get(name);

        return accounts.stream()
                .map(db::get)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Structure> findByValue(Double value) {
        Set<Long> accounts = valueIndex.get(value);

        return accounts.stream()
                .map(db::get)
                .collect(Collectors.toSet());
    }

    private void checkContainsAccount(Long account) {
        if (!db.containsKey(account)) {
            throw new StructureNotFoundException("Structure with account <" + account + "> not found!");
        }
    }

    private void checkNotContainsAccount(Long account) {
        if (db.containsKey(account)) {
            throw new AlreadyExistsException("Structure with account <" + account + "> already exists!");
        }
    }

}
